class AddTagToQuestions < ActiveRecord::Migration[5.1]
  def change
    add_reference :questions, :tag, foreign_key: true
  end
end
