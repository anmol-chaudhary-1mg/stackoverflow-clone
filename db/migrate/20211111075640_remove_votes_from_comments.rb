class RemoveVotesFromComments < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :votes, :string
  end
end
