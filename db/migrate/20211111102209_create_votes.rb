class CreateVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :votes do |t|
      t.integer :vote
      t.integer :votable_id
      t.string :votable_type
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
