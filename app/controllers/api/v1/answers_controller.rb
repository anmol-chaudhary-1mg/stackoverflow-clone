# frozen_string_literal: true

class Api::V1::AnswersController < BaseController
  private

  def structure
    @structure ||= Answer
  end

  def structure_params
    params.permit(:question_id, :content)
  end
end
