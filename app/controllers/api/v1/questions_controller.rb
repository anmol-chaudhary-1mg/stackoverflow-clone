# frozen_string_literal: true

class Api::V1::QuestionsController < BaseController

  private

  def structure
    @structure ||= Question
  end

  def structure_params
    params.permit(:title, :description)
  end

end
