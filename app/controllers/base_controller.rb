# frozen_string_literal: true

class BaseController < ApplicationController
  before_action :authenticate_user!

  def index
    loaded_structures = structure.order('created_at DESC')
    render json: { status: 'SUCCESS', message: 'Loaded Structure', data: loaded_structures }, status: :ok
  end

  def show
    loaded_structure = find_structure
    render json: { status: 'SUCCESS', message: 'Loaded Structure', data: loaded_structure }, status: :ok
  end

  def create
    question = structure.new(structure_params.merge(options))

    if question.save
      render json: { status: 'SUCCESS', message: 'Saved Structure', data: question }, status: :ok
    else
      render json: { status: 'ERROR', message: 'Structure not saved', data: question.errors },
             status: :unprocessable_entity
    end
  end

  def update
    loaded_structure = find_structure

    if loaded_structure
      loaded_structure.update(structure_params)
      render json: { status: 'SUCCESS', message: 'Updated Structure', data: loaded_structure }, status: :ok
    else
      render json: { status: 'ERROR', message: 'Structure not updated', data: loaded_structure.errors },
             status: :unprocessable_entity
    end
  end

  def destroy
    loaded_structure = find_structure

    if loaded_structure
      loaded_structure.destroy
      render json: { status: 'SUCCESS', message: 'Destroyed Structure', data: loaded_structure }, status: :ok
    else
      render json: { status: 'ERROR', message: 'Structure not destroyed', data: loaded_structure.errors },
             status: :unprocessable_entity
    end
  end

  private

  def structure
    raise NotImplementedError
  end

  def find_structure
    @article = structure.find(params[:id])
  end

  def structure_params
    raise NotImplementedError
  end

  def options
    @options ||= { user: current_user }
  end
end
