# frozen_string_literal: true

class  Question < ApplicationRecord
  belongs_to :user

  has_one :tag
  has_many :answers
  has_many :comments, as: :commentable
  has_many :votes, as: :votable
end
